package org.launchcode.service;

import org.launchcode.models.Course;
import org.launchcode.models.Role;
import org.launchcode.models.User;
import org.launchcode.models.data.CourseDao;
import org.launchcode.models.data.RoleDao;
import org.launchcode.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    RoleDao roleDao;

    @Autowired
    CourseDao courseDao;

    @Autowired
    UserDao userDao;

    public void addRole(User user, int roleId)
    {
        Role role = roleDao.findById(roleId).orElse(new Role());
        List<Role> my_roles = new ArrayList<>();
        my_roles.add(role);
        user.setRoles(my_roles);
    }

    public List<User> findAllProfessors()
    {
        Iterable<User> users = userDao.findAll();
        List<User> professors = new ArrayList<>();
        for (User u : users)
        {
            List<Role> roles = u.getRoles();
            for (Role r : roles)
                if (r.getName().equals("ROLE_PROFESSOR"))
                    professors.add(u);
        }
        return professors;
    }

    public List<User> findAllStudents()
    {
        Iterable<User> users = userDao.findAll();
        List<User> professors = new ArrayList<>();
        for (User u : users)
        {
            List<Role> roles = u.getRoles();
            for (Role r : roles)
                if (r.getName().equals("ROLE_STUDENT"))
                    professors.add(u);
        }
        return professors;
    }

    public void addCourseToStudent(User user, int courseId)
    {
        Course course = courseDao.findById(courseId).orElse(new Course());
        List<Course> cursuri = user.getCourses();
        if (cursuri == null)
            cursuri = new ArrayList<>();
        cursuri.add(course);
        user.setCourses(cursuri);
        userDao.save(user);
    }

    public void removeCourseStudent(User user, int courseId)
    {
        Course course = courseDao.findById(courseId).orElse(new Course());
        List<Course> cursuri = user.getCourses();
        if (cursuri == null)
            cursuri = new ArrayList<>();
        cursuri.remove(course);
        user.setCourses(cursuri);
        userDao.save(user);
    }

    public void addUser(User user, int roleId)
    {
        Role role = roleDao.findById(roleId).orElse(new Role());
        List<Role> my_roles = new ArrayList<>();
        my_roles.add(role);
        user.setRoles(my_roles);
        user.setEnabled(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

////////////////////////

    public List<User> allProfessors(){
        Iterable<User> users = userDao.findAll();
        List<User> professors = new ArrayList<>();

        for (User u : users)
            if ("Professor".equalsIgnoreCase(u.getRoles().get(0).getName()))
                professors.add(u);
        return professors;

    }

    public List<User> allStudents(){
        Iterable<User> users = userDao.findAll();
        List<User> students = new ArrayList<>();

        for (User u : users)
            if ("Student".equalsIgnoreCase(u.getRoles().get(0).getName()))
                students.add(u);
        return students;

    }

    public List<Course> getCourses(int userId)
    {
        User user = userDao.findById(userId).orElse(new User());
        List<Course> courses = user.getCourses();
        return courses;
    }

    public Role getUserRole(int userId)
    {
        User user = userDao.findById(userId).orElse(new User());
        Role role = user.getRoles().get(0);  //1 list
        return role;
    }

    /**
     * Metoda salveaza in baza de date un user
     * @param user este user-ul pe care dorim sa-l salvam in baza de date
     */

    public void save(User user)
    {
        userDao.save(user);
    }

    /**
     * Metoda sterge din baza de date un user cu id-ul furnizat ca parametru
     * @param id este id-ul obiectului din baza de date pe care dorim sa-l stergem
     */

    public void deleteById(int id)
    {
        userDao.deleteById(id);
    }

    /**
     * Metoda actualizeaza un user din baza de date
     * @param user este un obiect de tipul User care contine datele noi cu care vrem sa actualizam obiectul cu id ul id di baza de date
     * @param id este id-ul user-ului din baza de date pe care vrem sa-l actualizam
     */

    public void update(User user, int id)
    {
        User u = userDao.findById(id).orElse(new User());

        u.setEmail(user.getEmail());
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setUsername(user.getUsername());
        u.setPassword(user.getPassword());
        u.setCourses(user.getCourses());
        u.setEnabled(user.isEnabled());

        userDao.save(u);
    }


}
