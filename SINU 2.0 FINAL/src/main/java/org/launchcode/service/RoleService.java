package org.launchcode.service;

import org.launchcode.models.Role;
import org.launchcode.models.data.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {
    @Autowired
    private RoleDao roleDao;


    public List<Role> findAllButAdmin(){
        List<Role> my_roles = new ArrayList<>();
        for (Role r : roleDao.findAll())
            if (! r.getName().equals("ROLE_ADMIN"))
                my_roles.add(r);
        return my_roles;
    }

    public List<Role> findStudentRole(){
        List<Role> my_roles = new ArrayList<>();
        for (Role r : roleDao.findAll())
            if (r.getName().equals("ROLE_STUDENT"))
                my_roles.add(r);
        return my_roles;
    }

    public List<Role> findProfessorRole(){
        List<Role> my_roles = new ArrayList<>();
        for (Role r : roleDao.findAll())
            if (r.getName().equals("ROLE_PROFESSOR"))
                my_roles.add(r);
        return my_roles;
    }

    /////////////////////

    /**
     * Metoda salveaza in baza de date un rol
     * @param role este rolul pe care dorim sa-l salvam in baza de date
     */

    public void save(Role role)
    {
        roleDao.save(role);
    }

    /**
     * Metoda actualizeaza un rol din baza de date
     * @param role este un obiect de tipul Role care contine datele noi cu care vrem sa actualizam obiectul cu id ul id di baza de date
     * @param id este id-ul rolului din baza de date pe care vrem sa-l actualizam
     */

    public void update(Role role, int id)
    {
        Role r = roleDao.findById(id).orElse(new Role());

        r.setName(role.getName());
        r.setUsers(role.getUsers());

        roleDao.save(r);
    }

    /**
     * Metoda sterge din baza de date un rol cu id-ul furnizat ca parametru
     * @param id este id-ul obiectului din baza de date pe care dorim sa-l stergem
     */
    public void deleteById(int id)
    {
        roleDao.deleteById(id);
    }

}
