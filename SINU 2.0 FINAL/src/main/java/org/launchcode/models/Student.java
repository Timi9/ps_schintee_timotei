package org.launchcode.models;

import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class Student extends User {
    @ManyToMany(mappedBy = "students")
    private List<Professor> professors;

    public Student(){}

    public Student(@NotNull @Size(min = 3, max = 15) String firstName, @NotNull @Size(min = 3, max = 15) String lastName, @NotNull @Size(min = 3, max = 15) String username, @NotNull @Size(min = 3, max = 15) String password, @NotNull @Size(min = 3, max = 15) String email) {
        super(firstName, lastName, username, password, email);
    }


}
