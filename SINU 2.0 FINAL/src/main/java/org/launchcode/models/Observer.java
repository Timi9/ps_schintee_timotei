package org.launchcode.models;

public interface Observer {
    void update();
}

