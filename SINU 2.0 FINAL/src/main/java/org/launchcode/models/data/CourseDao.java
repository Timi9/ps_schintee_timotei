package org.launchcode.models.data;

import org.launchcode.models.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseDao extends CrudRepository<Course, Integer>  {
    Course findByName(String name);
}
