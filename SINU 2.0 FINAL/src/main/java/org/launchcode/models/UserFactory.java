package org.launchcode.models;

public class UserFactory {


    public User getUser(String userType) {
        if (userType == null) {
            return null;
        }
        if (userType.equalsIgnoreCase("Professor")) {
            return new Professor();

        } else if (userType.equalsIgnoreCase("Student")) {
            return new Student();

        } else {
            return new User();
        }


    }
}

