package org.launchcode.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Users")
public class User implements Observer{
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=3, max=15)
    private String firstName;

    @NotNull
    @Size(min=3, max=15)
    private String lastName;

    @NotNull
    @Size(min=3, max=15)
    @Column(unique=true)
    private String username;

    @NotNull
    @Size(min=3, max=100)
    private String password;

    @NotNull
    @Size(min=3, max=30)
    @Column(unique=true)
    @Pattern(regexp ="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
    private String email;

    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER) //am pus asta deoarece am avut o eroare in getAuthorities() fiindca tot ce are Many la sfarsit e LAZY LOADING
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private List<Role> roles;


    @ManyToMany
    @JoinTable(
            name = "users_courses",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "course_id", referencedColumnName = "id"))
    private List<Course> courses = null;

    public User(){
    }


    public User(@NotNull @Size(min = 3, max = 15) String firstName, @NotNull @Size(min = 3, max = 15) String lastName, @NotNull @Size(min = 3, max = 15) String username, @NotNull @Size(min = 3, max = 15) String password, @NotNull @Size(min = 3, max = 15) String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }


    @Override
    public void update() {
        System.out.println("A course has been changed. Take a look!");
    }
}
