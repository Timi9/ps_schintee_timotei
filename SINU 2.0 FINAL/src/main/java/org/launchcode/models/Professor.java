package org.launchcode.models;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class Professor extends User {
    @ManyToMany
    @JoinTable(
            name = "professor_student",
            joinColumns = @JoinColumn(
                    name = "professor_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "student_id", referencedColumnName = "id"))
    private List<Student> students = null;

    public Professor(){}

    public Professor(@NotNull @Size(min = 3, max = 15) String firstName, @NotNull @Size(min = 3, max = 15) String lastName, @NotNull @Size(min = 3, max = 15) String username, @NotNull @Size(min = 3, max = 15) String password, @NotNull @Size(min = 3, max = 15) String email) {
        super(firstName, lastName, username, password, email);
    }

    public int getRaport(){
        return students.size();
    }
}
