package org.launchcode.controllers;


import org.launchcode.models.Course;
import org.launchcode.models.Role;
import org.launchcode.models.User;
import org.launchcode.models.data.CourseDao;
import org.launchcode.models.data.UserDao;
import org.launchcode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "student")
public class StudentController {
    @Autowired
    UserDao userDao;

    @Autowired
    CourseDao courseDao;

    @Autowired
    UserService userService;


    @RequestMapping(value = "homepage")
    public String homepage() {
        return "student/homepage";
    }

    @RequestMapping(value = "allProfessors")
    public String allProfessors(Model model) {
        List<User> professors = userService.findAllProfessors();

        model.addAttribute("professors", professors);
        model.addAttribute("title","All professors");
        return "student/allProfessors";
    }

    @RequestMapping(value = "allCourses")
    public String allCourses(Model model) {
        Iterable<Course> courses = courseDao.findAll();

        model.addAttribute("courses", courses);
        model.addAttribute("title","All courses");
        return "student/allCourses";
    }

    @RequestMapping(value = "myCourses")
    public String myCourses(Model model, Authentication authentication) {

        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();
            User user = userDao.findByUsername(auth);
            List<Course> courses = user.getCourses();

            model.addAttribute("courses", courses);
            model.addAttribute("title","All courses");
            return "student/myCourses";
        }
        return "student/myCourses";
    }

    @RequestMapping(value = "addCourse", method = RequestMethod.GET)
    public String addCourse(Model model ) {
        model.addAttribute("courses", courseDao.findAll());
        return "student/addCourse";
    }

    @RequestMapping(value = "addCourse", method = RequestMethod.POST)
    public String processAddCourse(@RequestParam int courseId, Authentication authentication, Model model) {
        String auth = authentication.getName();
        User user = userDao.findByUsername(auth);
        userService.addCourseToStudent(user,courseId);

        model.addAttribute("message","SUCCESSFULLY ADDED!");
        return "redirect:/student/myCourses";
    }

    @RequestMapping(value = "removeCourse", method = RequestMethod.GET)
    public String removeCourse(Model model, Authentication authentication) {
        String auth = authentication.getName();
        User user = userDao.findByUsername(auth);
        model.addAttribute("courses", user.getCourses());
        return "student/removeCourse";
    }

    @RequestMapping(value = "removeCourse", method = RequestMethod.POST)
    public String removeCourseProc(@RequestParam int courseId, Authentication authentication, Model model) {
        String auth = authentication.getName();
        User user = userDao.findByUsername(auth);
        userService.removeCourseStudent(user,courseId);
        model.addAttribute("message","SUCCESSFULLY ADDED!");
        return "redirect:/student/myCourses";
    }

}
