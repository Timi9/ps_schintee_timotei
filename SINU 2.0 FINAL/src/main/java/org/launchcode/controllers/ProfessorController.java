package org.launchcode.controllers;

import org.launchcode.models.Course;
import org.launchcode.models.Role;
import org.launchcode.models.User;
import org.launchcode.models.data.CourseDao;
import org.launchcode.models.data.RoleDao;
import org.launchcode.models.data.UserDao;
import org.launchcode.service.RoleService;
import org.launchcode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "professor")
public class ProfessorController {


    @Autowired
    UserDao userDao;

    @Autowired
    CourseDao courseDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @RequestMapping(value = "homepage")
    public String homepage() {
        return "professor/homepage";
    }

    @RequestMapping(value = "allProfessors")
    public String allProfessors(Model model) {
        List<User> professors = userService.findAllProfessors();

        model.addAttribute("professors", professors);
        model.addAttribute("title","All professors");
        return "professor/allProfessors";
    }

    @RequestMapping(value = "allStudents")
    public String allStudents(Model model) {
        List<User> students = userService.findAllStudents();

        model.addAttribute("students", students);
        model.addAttribute("title","All students");
        return "professor/allStudents";
    }

    @RequestMapping(value = "allCourses")
    public String allCourses(Model model) {
        Iterable<Course> courses = courseDao.findAll();

        model.addAttribute("courses", courses);
        model.addAttribute("title","All courses");
        return "professor/allCourses";
    }



    @RequestMapping(value = "addStudent", method = RequestMethod.GET)
    public String displayAddStudentForm(Model model) {
        model.addAttribute(new User());
        List<Role> my_roles = roleService.findStudentRole();

        model.addAttribute("title","Register");
        model.addAttribute("roles", my_roles);
        return "/professor/addStudent";
    }

    @RequestMapping(value = "addStudent", method = RequestMethod.POST)
    public String processRegisterForm(@ModelAttribute @Valid User user, Errors errors, @RequestParam int roleId, Model model){
        /**
         *  MODEL BINDING
         *  Cheese newCheese = new Cheese();
         *  newCheese.setName(Request.getParameter("name"));
         *  newCheese.setDescription(Request.getParameter("description"));
         */
        if (errors.hasErrors()){
            List<Role> my_roles = roleService.findStudentRole();

            model.addAttribute("roles", my_roles);
            model.addAttribute("title","Register");
            return "/professor/addStudent";
        }

        userService.addUser(user,roleId);
        model.addAttribute("message","SUCCESSFUL ADDED!");
        return "/professor/addStudent";
    }

    //REGISTER
    @RequestMapping(value = "addCourse", method = RequestMethod.GET)
    public String displayAddCourseForm(Model model) {
        model.addAttribute("title","Add Course");
        model.addAttribute(new Course());
        return "/professor/addCourse";
    }

    @RequestMapping(value = "addCourse", method = RequestMethod.POST)
    public String processAddCourseForm(@ModelAttribute @Valid Course course, Errors errors, Model model){
        /**
         *  MODEL BINDING
         *  Cheese newCheese = new Cheese();
         *  newCheese.setName(Request.getParameter("name"));
         *  newCheese.setDescription(Request.getParameter("description"));
         */
        if (errors.hasErrors()){
            return "/professor/addCourse";
        }
        Course courseDB = courseDao.findByName(course.getName());
        if (courseDB==null)
        {
            courseDao.save(course);
            model.addAttribute("message","SUCCESSFUL ADDED!");
        }
        else
        {
            model.addAttribute("message","COURSE ALREADY EXISTS");
        }

        return "/professor/addCourse";
    }
}
