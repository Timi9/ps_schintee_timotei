package org.launchcode.controllers;

import org.launchcode.models.Course;
import org.launchcode.models.Role;
import org.launchcode.models.User;
import org.launchcode.models.data.CourseDao;
import org.launchcode.models.data.RoleDao;
import org.launchcode.models.data.UserDao;
import org.launchcode.service.RoleService;
import org.launchcode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "admin")
public class AdminController {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserDao userDao;

    @Autowired
    CourseDao courseDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @RequestMapping(value = "homepage")
    public String homepage() {
        return "admin/homepage";
    }


    @RequestMapping(value = "allProfessors")
    public String allProfessors(Model model) {
        List<User> professors = userService.findAllProfessors();

        model.addAttribute("professors", professors);
        model.addAttribute("title","All professors");
        return "admin/allProfessors";
    }

    @RequestMapping(value = "allStudents")
    public String allStudents(Model model) {
        List<User> students = userService.findAllStudents();

        model.addAttribute("students", students);
        model.addAttribute("title","All students");
        return "admin/allStudents";
    }

    @RequestMapping(value = "allCourses")
    public String allCourses(Model model) {
        Iterable<Course> courses = courseDao.findAll();

        model.addAttribute("courses", courses);
        model.addAttribute("title","All courses");
        return "admin/allCourses";
    }

    @RequestMapping(value = "allRoles")
    public String allRoles(Model model) {
        Iterable<Role> roles = roleDao.findAll();

        model.addAttribute("roles", roles);
        return "admin/allRoles";
    }

    @RequestMapping(value = "addStudent", method = RequestMethod.GET)
    public String displayAddStudentForm(Model model) {
        model.addAttribute(new User());
        List<Role> my_roles = roleService.findStudentRole();

        model.addAttribute("roles", my_roles);
        return "/admin/addStudent";
    }

    @RequestMapping(value = "addStudent", method = RequestMethod.POST)
    public String processRegisterForm(@ModelAttribute @Valid User user, Errors errors, @RequestParam int roleId, Model model){
        /**
         *  MODEL BINDING
         *  Cheese newCheese = new Cheese();
         *  newCheese.setName(Request.getParameter("name"));
         *  newCheese.setDescription(Request.getParameter("description"));
         */
        if (errors.hasErrors()){
            List<Role> my_roles = roleService.findStudentRole();
            model.addAttribute("roles", my_roles);
            model.addAttribute("title","Register");
            return "/admin/addStudent";
        }

        userService.addUser(user,roleId);
        model.addAttribute("message","SUCCESSFUL ADDED!");
        return "/admin/addStudent";
    }

    @RequestMapping(value = "addProfessor", method = RequestMethod.GET)
    public String displayAddProfForm(Model model) {
        model.addAttribute("title","Register");
        model.addAttribute(new User());
        List<Role> my_roles = roleService.findProfessorRole();
        model.addAttribute("roles", my_roles);
        return "/admin/addProfessor";
    }

    @RequestMapping(value = "addProfessor", method = RequestMethod.POST)
    public String processaddProfForm(@ModelAttribute @Valid User user, Errors errors, @RequestParam int roleId, Model model){
        /**
         *  MODEL BINDING
         *  Cheese newCheese = new Cheese();
         *  newCheese.setName(Request.getParameter("name"));
         *  newCheese.setDescription(Request.getParameter("description"));
         */
        if (errors.hasErrors()){
            List<Role> my_roles = roleService.findProfessorRole();
            model.addAttribute("roles", my_roles);
            model.addAttribute("title","Register");
            return "/admin/addProfessor";
        }

        userService.addUser(user,roleId);
        model.addAttribute("message","SUCCESSFUL ADDED!");
        return "/admin/addProfessor";
    }

    @RequestMapping(value = "addCourse", method = RequestMethod.GET)
    public String displayAddCourseForm(Model model) {
        model.addAttribute("title","Add Course");
        model.addAttribute(new Course());
        return "/admin/addCourse";
    }

    @RequestMapping(value = "addCourse", method = RequestMethod.POST)
    public String processAddCourseForm(@ModelAttribute @Valid Course course, Errors errors, Model model){
        /**
         *  MODEL BINDING
         *  Cheese newCheese = new Cheese();
         *  newCheese.setName(Request.getParameter("name"));
         *  newCheese.setDescription(Request.getParameter("description"));
         */
        if (errors.hasErrors()){
            model.addAttribute("title","Add Course");
            return "/admin/addCourse";
        }

        courseDao.save(course);
        model.addAttribute("message","SUCCESSFUL ADDED!");
        return "/admin/addCourse";
    }

    @RequestMapping(value = "addRole", method = RequestMethod.GET)
    public String displayAddRoleForm(Model model) {
        model.addAttribute("title","Add role");
        model.addAttribute(new Role());
        return "/admin/addRole";
    }

    @RequestMapping(value = "addRole", method = RequestMethod.POST)
    public String processAddrOLEForm(@ModelAttribute @Valid Role role, Errors errors, Model model){

        if (errors.hasErrors()){
            model.addAttribute("title","Add role");
            model.addAttribute(new Role());
            return "/admin/addRole";
        }

        roleDao.save(role);
        model.addAttribute("message","SUCCESSFUL ADDED!");
        return "/admin/addRole";
    }



    @RequestMapping(value = "removeStudent", method = RequestMethod.GET)
    public String removeStudent(Model model ) {
        List<User> students = userService.findAllStudents();

        model.addAttribute("users", students);
        return "admin/removeStudent";
    }

    @RequestMapping(value = "removeStudent", method = RequestMethod.POST)
    public String processRemoveStudent(@RequestParam int userId, Model model) {
        User user = userDao.findById(userId).orElse(new User());
        userDao.delete(user);
        return "redirect:/admin/allStudents";
    }

    @RequestMapping(value = "removeProfessor", method = RequestMethod.GET)
    public String removeProfessor(Model model ) {

        List<User> professors = userService.findAllProfessors();

        model.addAttribute("users", professors);
        return "admin/removeProfessor";
    }

    @RequestMapping(value = "removeProfessor", method = RequestMethod.POST)
    public String processRemoveProfessor(@RequestParam int userId, Model model) {
        User user = userDao.findById(userId).orElse(new User());
        userDao.delete(user);
        return "redirect:/admin/allProfessors";
    }


    @RequestMapping(value = "removeCourse", method = RequestMethod.GET)
    public String removeCourse(Model model ) {
        model.addAttribute("courses", courseDao.findAll());
        return "admin/removeCourse";
    }

    @RequestMapping(value = "removeCourse", method = RequestMethod.POST)
    public String processRemoveCourse(@RequestParam int courseId, Model model) {
        Course course = courseDao.findById(courseId).orElse(new Course());
        courseDao.delete(course);
        return "redirect:/admin/allCourses";
    }

   @RequestMapping(value = "removeRole", method = RequestMethod.GET)
    public String removeRole(Model model ) {
        model.addAttribute("roles", roleDao.findAll());
        return "admin/removeRole";
    }

    @RequestMapping(value = "removeRole", method = RequestMethod.POST)
    public String processRemoveRole(@RequestParam int roleId, Model model) {
        Role role = roleDao.findById(roleId).orElse(new Role());
        roleDao.delete(role);
        return "redirect:/admin/allRoles";
    }
}
