package org.launchcode.controllers;

import org.launchcode.models.Role;
import org.launchcode.models.User;
import org.launchcode.models.data.RoleDao;
import org.launchcode.models.data.UserDao;
import org.launchcode.service.RoleService;
import org.launchcode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@Controller
public class MainController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    RoleDao roleDao;

    @Autowired
    UserDao userDao;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "index")
    public String index(Model model) {
        model.addAttribute("title","Departamentul de Informatica");
        return "index";
    }

    @RequestMapping(value = "login",method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("title","Departamentul de Informatica");
        return "login";
    }


    @RequestMapping(value = "/logout")
    public String logout() {
        return "redirect:/login";
    }



    @RequestMapping(value = "login",method = RequestMethod.POST)
    public String plogin(Model model) {
        model.addAttribute("title","Departamentul de Informatica");
        return "register";
    }

    @RequestMapping(value = "/login-error.html")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login.html";
    }



    //REGISTER
    @RequestMapping(value = "register", method = RequestMethod.GET)
    public String displayRegisterForm(Model model) {
        model.addAttribute("title","Register");
        model.addAttribute(new User());
        List<Role> my_roles = roleService.findAllButAdmin();
        model.addAttribute("roles", my_roles);
        return "/register";
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String processRegisterForm(@ModelAttribute @Valid User user, Errors errors, @RequestParam int roleId, Model model){
        /**
         *  MODEL BINDING
         *  Cheese newCheese = new Cheese();
         *  newCheese.setName(Request.getParameter("name"));
         *  newCheese.setDescription(Request.getParameter("description"));
         */
        if (errors.hasErrors()){
            model.addAttribute("title","Register");
            List<Role> my_roles = roleService.findAllButAdmin();
            model.addAttribute("roles", my_roles);
            return "/register";
        }
        User userDB = userDao.findByUsername(user.getUsername());
        User userDBB = userDao.findByEmail(user.getEmail());
        if (userDB==null && userDBB==null) {
            userService.addUser(user,roleId);
            model.addAttribute("message", "SUCCESSFUL REGISTRATION!");
        }
        else if (userDB!=null)
        {
            model.addAttribute("title","Register");
            List<Role> my_roles = roleService.findAllButAdmin();
            model.addAttribute("roles", my_roles);
            model.addAttribute("message", "THIS USERNAME IS ALREADY TAKEN");
        }
        else
        {
            model.addAttribute("title","Register");
            List<Role> my_roles = roleService.findAllButAdmin();
            model.addAttribute("roles", my_roles);
            model.addAttribute("message", "THIS EMAIL IS ALREADY TAKEN");
        }
        return "/register";
    }




}
