package org.launchcode.controllers.rest;

import org.launchcode.models.Course;
import org.launchcode.models.data.CourseDao;
import org.launchcode.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
//@RequestMapping(value = "course")
public class CourseController {
    @Autowired
    CourseService courseService;

    @Autowired
    CourseDao courseDao;

    @RequestMapping(value = "/allCourses", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Iterable<Course> allCourses()
    {
        return courseDao.findAll();
    }

    @RequestMapping(value = "/createCourse", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Course createCourse(@RequestBody Course newCourse)
    {
        courseService.save(newCourse);
        return newCourse;
    }

    @RequestMapping(value = "/updateCourse/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public Course updateCourse(@RequestBody Course newCourse, @PathVariable int id)
    {
        courseService.update(newCourse,id);
        return newCourse;
    }

    @RequestMapping(value = "/deleteCourse/{id}", method = RequestMethod.DELETE, produces = "application/json")
    @ResponseBody
    public void deleteEmployee(@PathVariable int id) {
        courseService.deleteById(id);
    }


}