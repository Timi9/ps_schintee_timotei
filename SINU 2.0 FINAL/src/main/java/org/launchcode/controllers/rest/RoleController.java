package org.launchcode.controllers.rest;

import org.launchcode.models.Role;
import org.launchcode.models.data.RoleDao;
import org.launchcode.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class RoleController {
    @Autowired
    RoleService roleService;

    @Autowired
    RoleDao roleDao;

    @RequestMapping(value = "/allRoles", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Iterable<Role> allRoles()
    {
        return roleDao.findAll();
    }

    @RequestMapping(value = "/createRole", method = RequestMethod.POST)
    @ResponseBody
    public void createRole(@RequestBody Role newRole)
    {
        roleService.save(newRole);
    }

    @RequestMapping(value = "/updateRole/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public Role updateRole(@RequestBody Role newRole, @PathVariable int id)
    {
        roleService.update(newRole,id);
        return newRole;
    }

    @RequestMapping(value = "/deleteRole/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteRole(@PathVariable int id) {
        roleService.deleteById(id);
    }
}