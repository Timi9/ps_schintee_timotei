package org.launchcode.controllers.rest;


import org.launchcode.models.Course;
import org.launchcode.models.Role;
import org.launchcode.models.User;
import org.launchcode.models.data.UserDao;
import org.launchcode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
//RequestMapping(value = "user")
public class UserControllerRest {
    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    //materiile pe care le are un user
    @RequestMapping(value = "/getUsersCourses/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Course> getUsersCourses(@PathVariable int id)
    {
        return userService.getCourses(id);
    }

    //rolul unui user cu id ul dat
    @RequestMapping(value = "/getUserRole/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Role getUserRole(@PathVariable int id)
    {
        return userService.getUserRole(id);
    }



    @RequestMapping(value = "/allUsers", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Iterable<User> allUsers()
    {
        return userDao.findAll();
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    @ResponseBody
    public void createUser(@RequestBody User newUser)
    {
        userService.save(newUser);

    }

    @RequestMapping(value = "/updateUser/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public User updateUser(@RequestBody User newUser, @PathVariable int id)
    {
        userService.update(newUser,id);
        return newUser;
    }

    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteUser(@PathVariable int id) {
        userService.deleteById(id);
    }

    @RequestMapping(value = "/getAllProfessors/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> getAllProfessors()
    {

        return userService.allProfessors();
    }

    @RequestMapping(value = "/getAllStudents/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> getAllStudents()
    {

        return userService.allStudents();
    }
}
