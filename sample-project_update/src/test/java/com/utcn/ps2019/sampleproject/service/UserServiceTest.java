package com.utcn.ps2019.sampleproject.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.models.data.CourseDao;
import com.utcn.ps2019.sampleproject.models.data.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.yaml.snakeyaml.tokens.Token;

import javax.persistence.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService;

    @MockBean
    UserDao userDao;

    @Before
    public void init()
    {
        User user = new User("Ioan","Pop","popioan","abcd","ioan@gmail.com");
        List<Course> courses = new ArrayList<>();
        List<Role> roles = new ArrayList<>();
        roles.add(new Role("Professor"));
        courses.add(new Course("ASDN",5));
        user.setCourses(courses);
        user.setRoles(roles);

        when(userDao.findById(1)).thenReturn(Optional.of(user));

        User user1 = new User("Ioana","Popa","popioana","abcde","ioana@gmail.com");
        List<Role> roles1 = new ArrayList<>();
        roles.add(new Role("Student"));
        user.setRoles(roles1);

        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(user1);
        when(userDao.findAll()).thenReturn(users);

    }
    @Test
    public void getCourses()
    {
        List<Course> courses = userService.getCourses(1);
        verify(userDao.findById(1));
        assertEquals(courses.get(0).getName(),"ASDN");
    }

    @Test
    public void getUserRole()
    {
        Role role = userService.getUserRole(1);
        verify(userDao.findById(1));
        assertEquals(role.getName(),"Professor");
    }

    @Test
    public void allProfessors()
    {
        List<User> prof = userService.allProfessors();
        verify(userDao.findAll());
        assertEquals(prof.get(0).getFirstName(),"Ioan");
    }

    @Test
    public void allStudents()
    {
        List<User> stud = userService.allStudents();
        verify(userDao.findAll());
        assertEquals(stud.get(0).getFirstName(),"Ioana");
    }

    @Test
    public void save()
    {
        User user = new User();
        userService.save(user);
        verify(userDao.save(user));
    }

    @Test
    public void update()
    {
        User user = new User();
        userService.update(user, 1);
        verify(userDao.findById(1));
    }


}