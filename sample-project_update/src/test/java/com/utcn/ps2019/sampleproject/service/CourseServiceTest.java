package com.utcn.ps2019.sampleproject.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.models.data.CourseDao;
import com.utcn.ps2019.sampleproject.models.data.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.yaml.snakeyaml.tokens.Token;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseServiceTest {

    @Autowired
    CourseService courseService;

    @MockBean
    CourseDao courseDao;

    @Before
    public void init() {
        Course course1 = new Course("PC",5);
        Course course2 = new Course("ASDN",6);
        List<Course> courses = new ArrayList<>();
        courses.add(course2);
        courses.add(course1);

        Mockito.when(courseDao.findAll()).thenReturn(courses);
        Course course = new Course("MES",8);
        Mockito.when(courseDao.findById(15).orElse(new Course())).thenReturn(course);

    }

    @Test
    public void getCourseByCredits() {
        List<Course> courses = courseService.getCourseByCredits(5);
        verify(courseDao).findAll();
        assertEquals(courses.get(0).getName(),"PC");
    }
    @Test
    public void save()
    {
        Course course = new Course("ASDN",5);
        courseService.save(course);
        verify(courseDao).save(course);


    }
    @Test
    public void update()
    {
        Course course = new Course("MES",5);
        courseService.update(course,15);
        verify(courseDao).findById(15);
        verify(courseDao).save(course);
    }
    @Test
    public void deleteById()
    {
      Course course = new Course("CN",5);
      course.setId(10);
      courseService.deleteById(10);
      verify(courseDao).deleteById(10);

    }
}