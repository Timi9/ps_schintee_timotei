package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.data.CourseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {
    @Autowired
    private CourseDao courseDao;

    /**
     * Metoda salveaza in baza de date un curs
     * @param course este cursul pe care dorim sa-l salvam in baza de date
     */
    public void save(Course course)
    {
        courseDao.save(course);
    }


    /**
     * Metoda actualizeaza un curs din baza de date
     * @param course este un obiect de tipul Course care contine datele noi cu care vrem sa actualizam obiectul cu id ul id di baza de date
     * @param id este id-ul cursurilui din baza de date pe care vrem sa-l actualizam
     */
    public void update(Course course, int id)
    {
        Course c = courseDao.findById(id).orElse(new Course());
        c.setName(course.getName());
        c.setCredits(course.getCredits());
        courseDao.save(c);
    }


    /**
     * Metoda sterge din baza de date un curs cu id-ul furnizat ca parametru
     * @param id este id-ul obiectului din baza de date pe care dorim sa-l stergem
     */
    public void deleteById(int id)
    {
        courseDao.deleteById(id);
    }

    /**
     * Metoda returneaza cursurile din baza de date care au un numar de credite furnizat ca parametru
     * @param credits numarul de credite al cursurilor pe care le dorim
     * @return lista cu cursurile care au nr de credite dat ca parametru
     */
    public List<Course> getCourseByCredits(int credits)
    {
        Iterable<Course> courses = courseDao.findAll();  //st
        List<Course> myCourses = new ArrayList<>();
        for (Course c:courses)
        {
            if (c.getCredits()==credits)
                myCourses.add(c);
        }
        return myCourses;
    }

}
