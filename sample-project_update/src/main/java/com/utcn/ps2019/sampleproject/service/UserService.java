package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired     //di
    UserDao userDao;

    public List<User> allProfessors(){
        Iterable<User> users = userDao.findAll();
        List<User> professors = new ArrayList<>();

        for (User u : users)
            if ("Professor".equalsIgnoreCase(u.getRoles().get(0).getName()))
                professors.add(u);
        return professors;

    }

    public List<User> allStudents(){
        Iterable<User> users = userDao.findAll();
        List<User> students = new ArrayList<>();

        for (User u : users)
            if ("Student".equalsIgnoreCase(u.getRoles().get(0).getName()))
                students.add(u);
        return students;

    }

    public List<Course> getCourses(int userId)
    {
        User user = userDao.findById(userId).orElse(new User());
        List<Course> courses = user.getCourses();
        return courses;
    }

    public Role getUserRole(int userId)
    {
        User user = userDao.findById(userId).orElse(new User());
        Role role = user.getRoles().get(0);  //1 list
        return role;
    }

    /**
     * Metoda salveaza in baza de date un user
     * @param user este user-ul pe care dorim sa-l salvam in baza de date
     */

    public void save(User user)
    {
        userDao.save(user);
    }

    /**
     * Metoda sterge din baza de date un user cu id-ul furnizat ca parametru
     * @param id este id-ul obiectului din baza de date pe care dorim sa-l stergem
     */

    public void deleteById(int id)
    {
        userDao.deleteById(id);
    }

    /**
     * Metoda actualizeaza un user din baza de date
     * @param user este un obiect de tipul User care contine datele noi cu care vrem sa actualizam obiectul cu id ul id di baza de date
     * @param id este id-ul user-ului din baza de date pe care vrem sa-l actualizam
     */

    public void update(User user, int id)
    {
        User u = userDao.findById(id).orElse(new User());

        u.setEmail(user.getEmail());
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setUsername(user.getUsername());
        u.setPassword(user.getPassword());
        u.setCourses(user.getCourses());
        u.setEnabled(user.isEnabled());

        userDao.save(u);
    }

}
