package com.utcn.ps2019.sampleproject.controllers;

import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.models.data.RoleDao;
import com.utcn.ps2019.sampleproject.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

public class RoleController {
    @Autowired
    RoleService roleService;

    @Autowired
    RoleDao roleDao;

    @RequestMapping(value = "/allRoles", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Iterable<Role> allRoles()
    {
        return roleDao.findAll();
    }

    @RequestMapping(value = "/createRole", method = RequestMethod.POST)
    @ResponseBody
    public void createRole(@RequestBody Role newRole)
    {
        roleService.save(newRole);
    }

    @RequestMapping(value = "/updateRole/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public Role updateRole(@RequestBody Role newRole, @PathVariable int id)
    {
        roleService.update(newRole,id);
        return newRole;
    }

    @RequestMapping(value = "/deleteRole/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteRole(@PathVariable int id) {
        roleService.deleteById(id);
    }
}
