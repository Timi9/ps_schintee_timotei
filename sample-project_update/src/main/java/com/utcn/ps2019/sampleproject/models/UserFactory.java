package com.utcn.ps2019.sampleproject.models;

public class UserFactory {


    public User getUser(String UserType) {
        if (UserType == null) {
            return null;
        }
        if (UserType.equalsIgnoreCase("Professor")) {
            return new Professor();

        } else if (UserType.equalsIgnoreCase("Student")) {
            return new Student();

        } else {
            return new User();
        }


    }
}

