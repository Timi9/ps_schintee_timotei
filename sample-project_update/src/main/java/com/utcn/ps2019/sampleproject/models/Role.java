package com.utcn.ps2019.sampleproject.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    @Size(min=3, max=15)
    private String name;

    @JsonIgnore //din cauza unei exceptii
    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    public Role(@NotNull @Size(min = 3, max = 15) String name) {
        this.name = name;
    }

    public Role() {}

    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
