package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.models.data.CourseDao;
import com.utcn.ps2019.sampleproject.models.data.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseServiceTest {

    @Autowired
    CourseService courseService;

    @MockBean
    CourseDao courseDao;

    @Before
    public void init() {
        Course course1 = new Course("PC",5);
        Course course2 = new Course("ASDN",6);
        List<Course> courses = new ArrayList<>();
        courses.add(course2);
        courses.add(course1);

        Mockito.when(courseDao.findAll()).thenReturn(courses);
    }

    @Test
    public void getCourseByCredits() {
        List<Course> courses = courseService.getCourseByCredits(5);
        verify(courseDao).findAll();
        assertEquals(courses.get(0).getName(),"PC");
    }
}