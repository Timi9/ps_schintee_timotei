package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.data.CourseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {
    @Autowired
    private CourseDao courseDao;

    public void save(Course course)
    {
        courseDao.save(course);
    }

    public void update(Course course, int id)
    {
        Course c = courseDao.findById(id).orElse(new Course());
        c.setName(course.getName());
        c.setCredits(course.getCredits());
        courseDao.save(c);
    }

    public void deleteById(int id)
    {
        courseDao.deleteById(id);
    }

    public List<Course> getCourseByCredits(int credits)
    {
        Iterable<Course> courses = courseDao.findAll();  //st
        List<Course> myCourses = new ArrayList<>();
        for (Course c:courses)
        {
            if (c.getCredits()==credits)
                myCourses.add(c);
        }
        return myCourses;
    }

}
