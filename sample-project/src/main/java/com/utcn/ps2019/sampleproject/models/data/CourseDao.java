package com.utcn.ps2019.sampleproject.models.data;

import com.utcn.ps2019.sampleproject.models.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
@Transactional
public interface CourseDao extends CrudRepository<Course, Integer> {
    Course findByName(String name);
}