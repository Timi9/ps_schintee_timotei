package com.utcn.ps2019.sampleproject.models;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Student extends User {
    @ManyToMany(mappedBy = "students")
    private List<Professor> professors;

    public Student(){}

    public Student(@NotNull @Size(min = 3, max = 15) String firstName, @NotNull @Size(min = 3, max = 15) String lastName, @NotNull @Size(min = 3, max = 15) String username, @NotNull @Size(min = 3, max = 15) String password, @NotNull @Size(min = 3, max = 15) String email) {
        super(firstName, lastName, username, password, email);
    }

    public int getRaport()
    {
        return getCourses().size();
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    public void setFirstName(String firstName) {
        super.setFirstName(firstName);
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    @Override
    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public void setEmail(String email) {
        super.setEmail(email);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    @Override
    public List<Role> getRoles() {
        return super.getRoles();
    }

    @Override
    public void setRoles(List<Role> roles) {
        super.setRoles(roles);
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
    }

    @Override
    public List<Course> getCourses() {
        return super.getCourses();
    }

    @Override
    public void setCourses(List<Course> courses) {
        super.setCourses(courses);
    }
}
