package com.utcn.ps2019.sampleproject.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Course {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=2, max=15)
    @Column(unique=true)
    private String name;

    @NotNull
    @Positive
    private int credits;

    @ManyToMany(mappedBy = "courses")
    private List<User> users;

    public Course(@NotNull @Size(min = 3, max = 15) String name, @Positive @NotNull int credits) {
        this.name = name;
        this.credits = credits;
    }

    public Course () {}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

}
