package com.utcn.ps2019.sampleproject.controllers;

import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "user")
public class UserController {
    @Autowired
    UserService userService;

    //materiile pe care le are un user
    @RequestMapping(value = "/getUsersCourses/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Course> getUsersCourses(@PathVariable int id)
    {
        return userService.getCourses(id);
    }

    //rolul unui user cu id ul dat
    @RequestMapping(value = "/getUserRole/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Role getUserRole(@PathVariable int id)
    {
        return userService.getUserRole(id);
    }



}
