package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.models.Course;
import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired     //di
    UserDao userDao;

    public List<Course> getCourses(int userId)
    {
        User user = userDao.findById(userId).orElse(new User());
        List<Course> courses = user.getCourses();
        return courses;
    }

    public Role getUserRole(int userId)
    {
        User user = userDao.findById(userId).orElse(new User());
        Role role = user.getRoles().get(0);  //1 list
        return role;
    }
}
